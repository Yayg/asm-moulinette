#! /bin/sh

FICHIERS="AUTHORS README Makefile src"
RENDU=""
SRC=src

mkdir -p rendu test/ tarball

if [ -f arch ]; then
    echo ""
else
    echo ${FICHIERS} > arch
fi


for file in `echo tarball/*.tar.bz2`; do
    echo ${file}
    cp ${file} test/
    tar xf test/* -C test/
    rm -rf test/*.bz2
    ## AUTHORS exist
    if [ -f "test/AUTHORS" ]; then
        RENDU=`cat test/AUTHORS`
        ## AUTHORS have 1 line
        echo " `wc -l test/AUTHORS`" > test/tmp
        if grep -q -E " 1[[:space:]]" test/tmp; then
            ## AUTHORS match
            if grep -q -E "[*][[:space:]][[:alpha:]-]{1,6}_[[:alnum:]]$" test/AUTHORS; then
                echo "Correct AUTHORS "
                cp ./test-rendu.sh test/
                cd test; ./test-rendu.sh; cd ..
            else
                echo "Fail AUTHORS"
            fi
        else
            echo "AUTHORS with multiple lines"
        fi
    else
        echo "AUTHORS not found"
    fi
    echo ""
    rm -rf test/*
done
