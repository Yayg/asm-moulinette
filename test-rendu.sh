#! /bin/sh

arch=`cat ../arch`
arch=`echo ${arch}`

## Checking for dumb file
RENDU=`cat AUTHORS`
for str in `echo ${RENDU} src/*`; do
    echo ${str} > tmp
    if grep -q -E "[[:alpha:]-]{1,6}_[[:alnum:]]" tmp; then
        mkdir  ../rendu/${str} ../rendu/${str}/src
       ## Moving files
        for f in `echo ${arch}`; do
            if [ -f $f ]; then
                echo "File $f found."
                mv $f ../rendu/${str}/$f 2> /dev/null
            else
                echo "File $f not found."
            fi
        done
    elif grep -q -E ${str} ../arch; then
        wait
    elif test "test-rendu.sh" == ${str}; then
        wait
    elif test "tmp" == ${str}; then
        wait
    else
        echo "Dumb file or directory: ${str}"
    fi
    rm tmp
done
